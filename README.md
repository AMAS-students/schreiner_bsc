# Bachelor Thesis: A Performance Portable & Matrix Free Pre-Conditioner for CG

### Goal of the Thesis
In the project we will add matrix free, dimension independent pre-conditioners to the existing CG solver in IPPL. The prime equation we solve is of type Poisson in several dimensions with the possibility to do mixed-precision.
The project should be conducted using an incremental approach:
- first, we want to understand the state of the art in matrix free pre-conditioning and for this purpose we do a detailed literature review. A good starting point is the presentation by Kronbichler because he emphasises HPC. 
- motivate suitable pre-conditioners for our set of applications i.e. Alpine [1], and keep in mind, we are targeting large scale problems
- implementation and benchmark


### Milestones

- Implemented a matrix-free version of the Jacobian Preconditioner for the Laplacian differential operator. (26.09.2023)

### Code

The Code can be found in [this fork](https://github.com/bobschreiner/ippl) of the IPPL Library. 

