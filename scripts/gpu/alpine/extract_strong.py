import os
import csv

# Define the directory containing the files
directory = 'timings'  # Updated directory name

# Define the preconditioners
preconditioners = ['CG', 'jacobi', 'chebyshev', 'newton', 'gauss_seidel', 'richardson']

# Initialize a dictionary to store the data
data = {preconditioner: {} for preconditioner in preconditioners}

# Initialize a set to store all unique N values
unique_N_values = set()

# Iterate through the files
for file in os.listdir(directory):
    for preconditioner in preconditioners:
        if file.startswith('strong_scaling_' + preconditioner) and file.endswith('.out'):
            print('Processing file:' + file)  # Print the file name
            N = file.split('_')[3].split('.')[0]
            unique_N_values.add(N)
            with open(os.path.join(directory, file), 'r') as f:
                lines = f.readlines()
                for i, line in enumerate(lines):
                    if line.startswith('Timings{0}> solve............... Wall max =') or line.startswith('Timings> solve............... Wall max ='):
                        value = lines[i+1].split('=')[1].strip()
                        data[preconditioner][N] = value

# Write the data to a CSV file
csv_file = 'timing_data_all_preconditioners.csv'
with open(csv_file, mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['Nodes'] + preconditioners)
    for N in sorted(unique_N_values, key=int):  # Sort keys numerically
        row = [N] + [data[preconditioner].get(N, 'NaN') for preconditioner in preconditioners]
        writer.writerow(row)

print('Data has been saved to '+ csv_file)