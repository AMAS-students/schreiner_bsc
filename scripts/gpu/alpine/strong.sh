for i in 0 1 2 3
  do
  N=$((2**$i))
#  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_CG_${N}.out --error=strong_scaling_CG_${N}.err run.sh 256 256 256
#  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_newton_${N}.out --error=strong_scaling_newton_${N}.err run.sh 256 256 256 newton 5
#  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_chebyshev_${N}.out --error=strong_scaling_chebyshev_${N}.err run.sh 256 256 256 chebyshev 31
#  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_jacobi_${N}.out --error=strong_scaling_jacobi_${N}.err run.sh 256 256 256 jacobi
#  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_gauss-seidel_${N}.out --error=strong_scaling_gauss-seidel_${N}.err run.sh 256 256 256 gauss-seidel 5 2 1
#  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_gauss-seidel-no-comm_${N}.out --error=strong_scaling_gauss-seidel-no-comm_${N}.err run.sh 256 256 256 gauss-seidel 5 2 0
#  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_richardson_${N}.out --error=strong_scaling_richardson_${N}.err run.sh 256 256 256 richardson 8 1
  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_richardson-no-comm_${N}.out --error=strong_scaling_richardson-no-comm_${N}.err run.sh 256 256 256 richardson 8 0
done