#!/bin/bash
##SBATCH --error=LandauDamping_pascal_correctess.err
##SBATCH --output=LandauDamping_pascal_correctness.out
#SBATCH --time=00:20:00
#SBATCH --nodes=1
##SBATCH --ntasks=4
#SBATCH --clusters=gmerlin6
#SBATCH --partition=gpu-short
#SBATCH --exclusive
##SBATCH --gpus=4

N1=$1
N2=$2
N3=$3
preconditioner=$4
P1=$5
P2=$6
P3=$7
srun ./LandauDampingParameterList $N1 $N2 $N3 83886080 10 PCG $preconditioner $P1 $P2 $P3 0.05 --overallocate 1.0 --info 5 --kokkos-map-device-id-by=mpi_rank