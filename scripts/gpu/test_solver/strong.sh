for i in {0..3}
do
  N=$((2**$i))
  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_CG_${N}.out --error=strong_scaling_CG_${N}.err run.sh 8
  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_newton_${N}.out --error=strong_scaling_newton_${N}.err run.sh 8 newton 5
  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_chebyshev_${N}.out --error=strong_scaling_chebyshev_${N}.err run.sh 8 chebyshev 31
  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_jacobi_${N}.out --error=strong_scaling_jacobi_${N}.err run.sh 8 jacobi
  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_gauss-seidel_no_comm_${N}.out --error=strong_scaling_gauss-seidel_no_comm_${N}.err run.sh 8 gauss-seidel 5 5 0
  sbatch --gpus=$N --ntasks=$N --output=strong_scaling_richardson_no_comm_${N}.out --error=strong_scaling_richardson_no_comm_${N}.err run.sh 8 richardson 8 0
done