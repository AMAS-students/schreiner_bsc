import os
import csv

# Define the directory containing the files
directory = 'richardson'  # Updated directory name

# Define the preconditioners
preconditioners = set()

# Initialize a dictionary to store the data
timings = {}
iterations = {}
# Initialize a set to store all unique N values
unique_degree = set()


# Iterate through the files
for file in os.listdir(directory):
    #for preconditioner in preconditioners:
        if file.startswith('gauss') and file.endswith('.out'):
            print('Processing file:' + file)  # Print the file name
            preconditioner = file.split('_')[0]
            degree = file.split('_')[-1].split('.')[0]
            unique_degree.add(degree)
            if preconditioner not in timings:
                timings[preconditioner] = {}
                iterations[preconditioner] = {}
            preconditioners.add(preconditioner)
            with open(os.path.join(directory, file), 'r') as f:
                lines = f.readlines()
                for i, line in enumerate(lines):
                    if line.startswith('Timings{0}> solve............... Wall max =') or line.startswith('Timings> solve............... Wall max ='):
                        time = lines[i+1].split('=')[1].strip()
                        timings[preconditioner][degree] = time
                    elif line.startswith('Convergence{0}>') or line.startswith('Convergence>'):
                         iteration = line.split(',')[-1].strip()
                         iterations[preconditioner][degree] = iteration

# Write the data to a CSV file
csv_file = 'richardson_degree.csv'
with open(csv_file, mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['preconditioner' , 'degree' , 'time' , 'iterations'])
    for preconditioner in preconditioners :
            for degree in sorted(unique_degree, key=int):
                row = [preconditioner ,degree , timings[preconditioner].get(degree, 'NaN') , iterations[preconditioner].get(degree, 'NaN')]
                writer.writerow(row)

print('Data has been saved to '+ csv_file)