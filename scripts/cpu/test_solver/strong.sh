for i in {0..3}
do
N=$((2**$i))
sbatch --nodes=$N --ntasks=$N --output=strong_scaling_CG_${N}.out --error=strong_scaling_CG_${N}.err run.sh 8
sbatch --nodes=$N --ntasks=$N --output=strong_scaling_newton_${N}.out --error=strong_scaling_newton_${N}.err run.sh newton 5
sbatch --nodes=$N --ntasks=$N --output=strong_scaling_chebyshev_${N}.out --error=strong_scaling_chebyshev_${N}.err run.sh chebyshev 31
sbatch --nodes=$N --ntasks=$N --output=strong_scaling_jacobi_${N}.out --error=strong_scaling_jacobi_${N}.err run.sh jacobi
sbatch --nodes=$N --ntasks=$N --output=strong_scaling_gauss-seidel-no-comm_${N}.out --error=strong_scaling_gauss-seidel-no-comm_${N}.err run.sh gauss-seidel 5 2 0
sbatch --nodes=$N --ntasks=$N --output=strong_scaling_gauss-seidel_${N}.out --error=strong_scaling_gauss-seidel_${N}.err run.sh gauss-seidel 5 2 1
sbatch --nodes=$N --ntasks=$N --output=strong_scaling_richardson-no-comm_${N}.out --error=strong_scaling_richardson-no-comm_${N}.err run.sh richardson 8 0
sbatch --nodes=$N --ntasks=$N --output=strong_scaling_richardson_${N}.out --error=strong_scaling_richardson_${N}.err run.sh richardson 8 1
done
 