#!/bin/bash
#SBATCH --partition=hourly       # Using 'hourly' will grant higher priority
##SBATCH --nodes=8               # number of nodes
##SBATCH --ntasks=8              # Total No. of MPI ranks
#SBATCH --ntasks-per-node=1      # No. of MPI ranks per node. Merlin CPU nodes have 44 cores
#SBATCH --ntasks-per-core=1      # Force no Hyper-Threading, will run 1 task per core
#SBATCH --cpus-per-task=44        # No. of OMP threads
#SBATCH --time=01:00:00          # Define max time job will run
#SBATCH --exclusive              # The allocations will be exclusive if turned on
##SBATCH --output=testCG.out     # Define your output file
##SBATCH --error=testCG.err      # Define your error file
#SBATCH --hint=nomultithread     # No hyperthreading

export OMP_NUM_THREADS=8
export OMP_PROC_BIND=spread
export OMP_PLACES=threads


N=$1
scaling=$2
preconditioner=$3
P1=$4
P2=$5
P3=$6
srun ./TestCGSolver $N $scaling $preconditioner $P1 $P2 $P3 --info 5