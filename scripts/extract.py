import os
import re
import csv

# Define the directory containing the files
directory = 'data'

# Get a list of all files in the directory
files = os.listdir(directory)

# Define the pattern to search for Wall max in solve
pattern = r"solve\s+\.{3}\s+Wall max =\s+([\d.e-]+)"

# Initialize a dictionary to store the data
data = {}

# Iterate through the files
for file in files:
    if file.endswith('.out') and file.startswith('strong_'):
        preconditioner = file.split('_')[1]  # Extract preconditioner from file name
        N = file.split('_')[2].split('.')[0]  # Extract N from file name
        with open(os.path.join(directory, file), 'r') as f:
            text = f.read()
            matches = re.findall(pattern, text)
            if matches:
                value = matches[0]
                if N not in data:
                    data[N] = {}
                data[N][preconditioner] = value

# Write the data to a CSV file
csv_file = 'timing_data.csv'
with open(csv_file, mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['Nodes', 'CG', 'Jacobi', 'Chebyshev', 'Newton'])
    for N, values in data.items():
        row = [N, values.get('CG', 'NaN'), values.get('jacobi', 'NaN'), values.get('chebyshev', 'NaN'), values.get('newton', 'NaN')]
        writer.writerow(row)

print(f'Data has been saved to {csv_file}')