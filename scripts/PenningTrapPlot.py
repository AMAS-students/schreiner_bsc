import pyvista as pv
import numpy as np
import imageio
import os
import concurrent.futures

pv.global_theme.allow_empty_mesh = True

# Directory containing your .vtk files
directory = '/Users/bobschreiner/ETH/Thesis/ippl/build_serial/alpine/data'  # replace with your actual directory
pngdirectory = '/Users/bobschreiner/ETH/Thesis/schreiner_bsc/scripts/PenningTrapPNG/'

# Create a plotter object
plotter = pv.Plotter(off_screen=True)

def process_file(i):
    # Format the file number to have four digits
    file_number = str(i).zfill(4)

    # Path to the .vtk file
    file_path = os.path.join(directory, f'scalar_{file_number}.vtk')

    # Load the .vtk file
    grid = pv.read(file_path)

    # Apply a threshold filter to include only cells with a scalar value less than 0
    grid["Rho"] = -grid["Rho"]

    grid.threshold(10, scalars='Rho', invert=True)


    # Filter the grid to include only points with x-coordinate less than or equal to half of the maximum x-coordinate
    half_grid_x = grid.clip(origin=[grid.points[:, 0].max() / 2, 0, 0], normal='x')

    # Clip the grid along the y-axis at half of the maximum y-coordinate
    half_grid_y = half_grid_x.clip(origin=[0, grid.points[:, 1].max() / 2, 0], normal='y')

    # Clip the grid along the z-axis at half of the maximum z-coordinate
    half_grid_z = half_grid_y.clip(origin=[0, 0, grid.points[:, 2].max() / 2], normal='z')

    # Check if the mesh is not empty
    if grid.n_cells > 0:

        # Add the surface to the plotter
        plotter.add_mesh(half_grid_z, scalars='Rho',cmap='viridis', clim=[10 , 70])
        xmin , xmax = 0.0 , 15
        ymin , ymax = 0.0 , 15
        zmin , zmax = 0.0 , 15

        # Set the range of the x, y, and z axes
        # plotter.view_x_range = [xmin, xmax]
        # plotter.view_y_range = [ymin, ymax]
        # plotter.view_z_range = [zmin, zmax]

        # Path to the .png file
        png_file_path = os.path.join(pngdirectory, f'screenshot_{file_number}.png')

        # Capture a screenshot and save it as a .png file
        plotter.screenshot(png_file_path)

        return png_file_path

def make_gif():
    # List to store the file paths of the PNG files
    png_files = []

    # Use a ThreadPoolExecutor to process multiple .vtk files at the same time
    with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
        for png_file_path in executor.map(process_file, range(100,301)):
            if png_file_path is not None:
                png_files.append(png_file_path)

    # Create a GIF from the PNG files
    imageio.mimsave(os.path.join(pngdirectory, 'PenningTrap.gif'), [imageio.imread(file) for file in png_files])

def make_snapshot(i):
    # Format the file number to have four digits
    file_number = str(i).zfill(4)

    # Path to the .vtk file
    file_path = os.path.join(directory, f'scalar_{file_number}.vtk')

    # Load the .vtk file
    grid = pv.read(file_path)

    grid["Rho"] = -grid["Rho"]

    projected_grid = grid.slice(normal=[0, 0, 1])

    plotter.add_mesh(projected_grid, scalars='Rho')

    # Path to the .png file
    png_file_path = os.path.join(pngdirectory, f'screenshot_{file_number}.png')

    # Capture a screenshot and save it as a .png file
    plotter.screenshot(png_file_path)

    plotter.show(screenshot=f'screenshot_{file_number}.png')

def main():
    make_gif()
    #make_snapshot(300)


if __name__ == '__main__':
    main()